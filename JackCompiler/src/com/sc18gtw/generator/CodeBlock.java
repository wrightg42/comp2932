package com.sc18gtw.generator;

/**
 * The type Code block.
 */
public class CodeBlock {
    /**
     * Instantiates a new Code block.
     *
     * @param code     the compiled code block
     * @param returned whether the code returned
     */
    public CodeBlock(String code, boolean returned) {
        this.code = code;
        this.returned = returned;
    }

    private String code;
    private boolean returned;

    /**
     * Gets the compiled code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Is returned boolean.
     *
     * @return the boolean
     */
    public boolean isReturned() {
        return returned;
    }
}
