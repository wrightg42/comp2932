package com.sc18gtw.generator.symboltable;

/**
 * The type symbol data.
 */
public class TypeSymbol {
    /**
     * Instantiates a new Type symbol.
     *
     * @param size the size
     */
    public TypeSymbol(int size) {
        this.size = size;
    }

    private int size;

    /**
     * Gets the size of the type.
     *
     * @return the size
     */
    public int getSize() {
        return size;
    }
}
