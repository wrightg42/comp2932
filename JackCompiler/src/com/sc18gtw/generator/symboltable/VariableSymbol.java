package com.sc18gtw.generator.symboltable;

/**
 * The Symbol data structure.
 */
public class VariableSymbol {
    /**
     * The enum Symbol segment types.
     */
    public enum SegmentTypes {
        /**
         * Static segment.
         */
        STATIC ("static"),
        /**
         * Argument segment.
         */
        ARGUMENT ("argument"),
        /**
         * Var segment.
         */
        LOCAL ("local"),
        /**
         * This segment.
         */
        THIS ("this");

        private SegmentTypes(String type) {
            this.type = type;
        }

        private final String type;

        @Override
        public String toString() {
            return this.type;
        }
    }

    /**
     * Instantiates a new Variable symbol.
     *
     * @param type   the type
     * @param offset the offset
     */
    public VariableSymbol(String type, int offset, SegmentTypes segment) {
        this.type = type;
        this.offset = offset;
        this.segment = segment;
        initialised = false;
    }

    private String type;
    private int offset;
    private SegmentTypes segment;
    private boolean initialised;

    /**
     * Gets type.
     *
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * Gets offset.
     *
     * @return the offset
     */
    public int getOffset() {
        return offset;
    }

    /**
     * Gets segment type.
     *
     * @return the segment of the symbol.
     */
    public SegmentTypes getSegment() {
        return segment;
    }

    /**
     * Is initialised boolean.
     *
     * @return the boolean
     */
    public boolean isInitialised() {
        return initialised;
    }

    /**
     * Sets to initialised.
     */
    public void setToInitialised() {
        initialised = true;
    }
}
