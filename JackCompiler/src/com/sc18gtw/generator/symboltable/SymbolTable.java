package com.sc18gtw.generator.symboltable;

import com.sc18gtw.generator.exceptions.ExistingSymbolException;
import com.sc18gtw.generator.exceptions.UnrecognisedSymbolException;
import com.sc18gtw.parser.syntaxtree.SyntaxTreeNode;
import com.sc18gtw.parser.syntaxtree.SyntaxTreeTerminal;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * The Symbol table construct.
 */
public class SymbolTable {
    /**
     * Instantiates a new Symbol table.
     *
     * @param segment the segment this symbol table represents.
     */
    public SymbolTable(VariableSymbol.SegmentTypes segment) {
        table = new HashMap<>();
        offset = 0;
        this.segment = segment;
    }

    private HashMap<String, VariableSymbol> table;
    private int offset;
    private VariableSymbol.SegmentTypes segment;

    /**
     * Inserts a variable into the symbol table.
     *
     * @param variableNode the variable deceleration node to add
     */
    public void insert(SyntaxTreeNode variableNode) throws ExistingSymbolException, UnrecognisedSymbolException {
        insert(variableNode, "");
    }

    /**
     * Inserts a variable into the symbol table.
     *
     * @param variableNode the variable deceleration node to add
     * @param classname    the classname of the symbol to add. Must contain the '.'
     */
    public void insert(SyntaxTreeNode variableNode, String classname) throws ExistingSymbolException, UnrecognisedSymbolException {
        int index = 0; // where the type child node will be
        if (variableNode.getType() == SyntaxTreeNode.SyntaxTypes.CLASS_VAR) {
            ++index;
        }

        // get type
        String type = variableNode.getChild(index).toString();
        if (!TypeTable.lookup(type)) {
            throw new UnrecognisedSymbolException(((SyntaxTreeTerminal) variableNode.getChild(index)).getToken(), "type");
        }

        // add variables
        for (++index; index < variableNode.countChildren(); ++index) {
            insert(classname, (SyntaxTreeTerminal) variableNode.getChild(index), type);
        }
    }

    private void insert(String classname, SyntaxTreeTerminal terminal, String type) throws ExistingSymbolException {
        VariableSymbol newVariableSymbol = new VariableSymbol(type, offset, segment);
        ++offset;
        //offset += TypeTable.getSymbol(type).getSize();

        VariableSymbol prev = table.put(classname + terminal.toString(), newVariableSymbol);
        if (prev != null) {
            throw new ExistingSymbolException(terminal.getToken(), "variable");
        }
    }

    /**
     * Lookup whether a symbol exists.
     *
     * @param name the symbol to lookup
     * @return whether the symbol exists
     */
    public boolean lookup(String name) {
        return getSymbol(name) != null;
    }

    /**
     * Gets a symbol from the symbol table.
     *
     * @param name the symbol to get
     * @return the symbol
     */
    public VariableSymbol getSymbol(String name) {
        return table.get(name);
    }

    /**
     * Checks whether a symbol is initialised.
     *
     * @param name the symbol to check
     * @return whether the symbol is initialised
     */
    public boolean isInitialised(String name) {
        return getSymbol(name).isInitialised();
    }

    /**
     * Sets a symbol to be initialised.
     *
     * @param name the symbol name
     */
    public void setToInitialised(String name) {
        VariableSymbol symbol = getSymbol(name);
        symbol.setToInitialised();
        table.put(name, symbol);
    }

    /**
     * Gets the size of the symbol table.
     *
     * @return the size of the table
     */
    public int size() {
        return table.size();
    }

    /**
     * Clear's the symbol table's data.
     */
    public void clear() {
        table.clear();
        offset = 0;
    }

    /**
     * The symbol table for existing types.
     */
    public static class TypeTable {
        private static HashMap<String, TypeSymbol> table;

        static {
            table = new HashMap<>();

            // add primitive types to table
            TypeSymbol basicType = new TypeSymbol(1);
            table.put("int", basicType);
            table.put("char", basicType);
            table.put("boolean", basicType);

            // add standard library types
            table.put("Array", basicType);
            table.put("String", basicType); // these should be different for different size n?

            // add standard library types that cannot be initialized, prevents new classes with same names being made
            table.put("Math", basicType);
            table.put("Memory", basicType);
            table.put("Screen", basicType);
            table.put("Keyboard", basicType);
            table.put("Output", basicType);
            table.put("Sys", basicType);
        }

        /**
         * Inserts an item into the type table.
         *
         * @param classNode the class node to insert into the type table
         */
        public static void insert(SyntaxTreeNode classNode) throws ExistingSymbolException {
            // get how many fields, thus size of object
            int size = 0;
            for (SyntaxTreeNode field : classNode.getChild(1).getChild(0).getChildren()) {
                if (field.getChildren().get(0).toString().equals("field")) {
                    size += field.countChildren() - 2;
                }
            }

            // add symbol to type table
            TypeSymbol newType = new TypeSymbol(size);
            TypeSymbol prev = table.put(classNode.getChild(0).toString(), newType);
            if (prev != null) {
                throw new ExistingSymbolException(((SyntaxTreeTerminal) classNode.getChild(0)).getToken(), "class");
            }
        }

        /**
         * Lookup whether a type exists.
         *
         * @param name the type's name
         * @return whether the type exists
         */
        public static boolean lookup(String name) {
            return getSymbol(name) != null;
        }

        /**
         * Gets a type's data.
         *
         * @param name the type's name
         * @return the type data
         */
        public static TypeSymbol getSymbol(String name) {
            return table.get(name);
        }
    }

    /**
     * The symbol table for existing functions.
     */
    public static class FunctionTable {
        private static HashMap<String, FunctionSymbol> functionTable;
        private static HashMap<String, FunctionSymbol> methodTable;

        static  {
            functionTable = new HashMap<>();
            methodTable = new HashMap<>();

            // add math standard library methods
            functionTable.put("Math.abs", new FunctionSymbol(true, "int", "int"));
            functionTable.put("Math.multiply", new FunctionSymbol(true, "int", "int", "int"));
            functionTable.put("Math.divide", new FunctionSymbol(true, "int", "int", "int"));
            functionTable.put("Math.min", new FunctionSymbol(true, "int", "int", "int"));
            functionTable.put("Math.max", new FunctionSymbol(true, "int", "int", "int"));
            functionTable.put("Math.sqrt", new FunctionSymbol(true, "int", "int"));

            // add array standard library methods
            functionTable.put("Array.new", new FunctionSymbol(true, "Array", "int"));
            methodTable.put("Array.dispose", new FunctionSymbol(false, "void"));

            // add memory standard library methods
            functionTable.put("Memory.peek", new FunctionSymbol(true, "int", "int"));
            functionTable.put("Memory.poke", new FunctionSymbol(true, "void", "int", "int"));
            functionTable.put("Memory.alloc", new FunctionSymbol(true, "Array", "int"));
            functionTable.put("Memory.deAlloc", new FunctionSymbol(true, "void", "Array"));

            // add screen standard library methods
            functionTable.put("Screen.clearScreen", new FunctionSymbol(true, "void"));
            functionTable.put("Screen.setColor", new FunctionSymbol(true, "void", "boolean"));
            functionTable.put("Screen.drawPixel", new FunctionSymbol(true, "void", "int", "int"));
            functionTable.put("Screen.drawLine", new FunctionSymbol(true, "void", "int", "int", "int", "int"));
            functionTable.put("Screen.drawRectangle", new FunctionSymbol(true, "void", "int", "int", "int", "int"));
            functionTable.put("Screen.drawCircle", new FunctionSymbol(true, "void", "int", "int", "int"));

            // add keyboard standard library methods
            functionTable.put("Keyboard.keyPressed", new FunctionSymbol(true, "char"));
            functionTable.put("Keyboard.readChar", new FunctionSymbol(true, "char"));
            functionTable.put("Keyboard.readLine", new FunctionSymbol(true, "String", "String"));
            functionTable.put("Keyboard.readInt", new FunctionSymbol(true, "int", "String"));

            // add output standard library methods
            functionTable.put("Output.init", new FunctionSymbol(true, "void"));
            functionTable.put("Output.moveCursor", new FunctionSymbol(true, "void", "int", "int"));
            functionTable.put("Output.printChar", new FunctionSymbol(true, "void", "char"));
            functionTable.put("Output.printString", new FunctionSymbol(true, "void", "String"));
            functionTable.put("Output.printInt", new FunctionSymbol(true, "void", "int"));
            functionTable.put("Output.println", new FunctionSymbol(true, "void"));
            functionTable.put("Output.backSpace", new FunctionSymbol(true, "void"));

            // add string standard library methods
            functionTable.put("String.new", new FunctionSymbol(true, "String", "int"));
            methodTable.put("String.dispose", new FunctionSymbol(false, "void"));
            methodTable.put("String.length", new FunctionSymbol(false, "int"));
            methodTable.put("String.charAt", new FunctionSymbol(false, "char", "int"));
            methodTable.put("String.setCharAt", new FunctionSymbol(false, "void", "int", "char"));
            methodTable.put("String.appendChar", new FunctionSymbol(false, "String", "char"));
            methodTable.put("String.eraseLastChar", new FunctionSymbol(false, "void"));
            methodTable.put("String.intValue", new FunctionSymbol(false, "int"));
            methodTable.put("String.setInt", new FunctionSymbol(false, "void", "int"));
            functionTable.put("String.newLine", new FunctionSymbol(true, "char"));
            functionTable.put("String.backSpace", new FunctionSymbol(true, "char"));
            functionTable.put("String.doubleQuote", new FunctionSymbol(true, "char"));

            // add sys standard library methods
            functionTable.put("Sys.halt", new FunctionSymbol(true, "void"));
            functionTable.put("Sys.error", new FunctionSymbol(true, "void", "int"));
            functionTable.put("Sys.wait", new FunctionSymbol(true, "void", "int"));
        }

        /**
         * Inserts an item into the function table.
         *
         * @param functionNode the function node to insert into the function table
         * @param classname    the classname that the function belongs to. Must contain the '.'
         */
        public static void insert(SyntaxTreeNode functionNode, String classname) throws ExistingSymbolException, UnrecognisedSymbolException {
            // get if method or function/constructor, and identifier
            boolean method = functionNode.getChild(0).toString().equals("method");
            String name = classname + functionNode.getChild(2).toString();

            // get return type and validate type
            String retType = functionNode.getChild(1).toString();
            if (!retType.equals("void") && !TypeTable.lookup(retType)) {
                throw new UnrecognisedSymbolException(((SyntaxTreeTerminal) functionNode.getChild(1)).getToken(), "type");
            }

            // get param types
            List<String> paramTypesList = new LinkedList<>();
            for (SyntaxTreeNode param : functionNode.getChild(3).getChildren()) {
                // validate recognised type
                String paramType = param.getChild(0).toString();
                if (!TypeTable.lookup(paramType)) {
                    throw new UnrecognisedSymbolException(((SyntaxTreeTerminal) param.getChild(0)).getToken(), "type");
                }

                paramTypesList.add(param.getChild(0).toString());
            }

            String[] paramTypes = new String[paramTypesList.size()];
            paramTypesList.toArray(paramTypes);

            // add to function lists
            FunctionSymbol newFunction = new FunctionSymbol(!method, retType, paramTypes);
            FunctionSymbol prev;
            if (method) {
                prev = methodTable.put(name, newFunction);
            } else {
                prev = functionTable.put(name, newFunction);
            }

            if (prev != null) {
                throw new ExistingSymbolException(((SyntaxTreeTerminal) functionNode.getChild(2)).getToken(),
                        method ? "method" : "function");
            }
        }

        /**
         * Lookup whether a function exists.
         *
         * @param name the name of the function
         * @return whether the function exists
         */
        public static boolean lookupFunction(String name) {
            return getFunctionSymbol(name) != null;
        }

        /**
         * Gets a functions's data.
         *
         * @param name the name of the function
         * @return the function symbol
         */
        public static FunctionSymbol getFunctionSymbol(String name) {
            return functionTable.get(name);
        }

        /**
         * Gets a method's data.
         *
         * @param name the name of the method
         * @return whether the method exists
         */
        public static boolean lookupMethod(String name) {
            return getMethodSymbol(name) != null;
        }

        /**
         * Gets a method's data.
         *
         * @param name the name of the method
         * @return the method symbol
         */
        public static FunctionSymbol getMethodSymbol(String name) {
            return methodTable.get(name);
        }
    }
}
