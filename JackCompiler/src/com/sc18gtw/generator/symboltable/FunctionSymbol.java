package com.sc18gtw.generator.symboltable;

/**
 * The function symbol data.
 */
public class FunctionSymbol {
    /**
     * Instantiates a new Function symbol.
     *
     * @param isFunction whether the symbol is a function
     * @param returnType the return type
     * @param argTypes   the argument types
     */
    public FunctionSymbol(boolean isFunction, String returnType, String... argTypes) {
        this.isFunction = isFunction;
        this.argTypes = argTypes;
        this.returnType = returnType;
    }

    private boolean isFunction;
    private String[] argTypes;
    private String returnType;

    /**
     * Is function boolean.
     *
     * @return whether the symbol is a function
     */
    public boolean isFunction() {
        return isFunction;
    }

    /**
     * Gets the amount of argument.
     *
     * @return the amount of arguments
     */
    public int getArgsCount() {
        return argTypes.length;
    }

    /**
     * Get argument types.
     *
     * @return the array of argument types.
     */
    public String[] getArgTypes() {
        return argTypes;
    }

    /**
     * Gets return type.
     *
     * @return the return type
     */
    public String getReturnType() {
        return returnType;
    }
}
