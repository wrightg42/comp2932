package com.sc18gtw.generator;

import com.sc18gtw.exceptions.ExceptionFromFile;
import com.sc18gtw.generator.exceptions.*;
import com.sc18gtw.generator.symboltable.FunctionSymbol;
import com.sc18gtw.generator.symboltable.SymbolTable;
import com.sc18gtw.generator.symboltable.VariableSymbol;
import com.sc18gtw.lexer.Token;
import com.sc18gtw.parser.Parser;
import com.sc18gtw.parser.syntaxtree.SyntaxTree;
import com.sc18gtw.parser.syntaxtree.SyntaxTreeNode;
import com.sc18gtw.parser.syntaxtree.SyntaxTreeOperationNode;
import com.sc18gtw.parser.syntaxtree.SyntaxTreeTerminal;

import java.io.*;
import java.util.*;

/**
 * The Compiler's code generator.
 */
public class Generator {
    private static String directory;
    private static List<SymbolTable> symbolTables;
    private static HashMap<String, Integer> symbolTableIndex;
    private static String currentFile;
    private static String currentReturnType;
    private static String currentClassname;
    private static String currentLabel;
    private static List<Throwable> warnings;
    private static HashMap<String, String> outputFiles;
    private static final int STATIC_TABLE = 0;
    private static final int ARGUMENT_TABLE = 1;
    private static final int LOCAL_TABLE = 2;
    private static final boolean strictTypes = false;

    /**
     * Compiles source code in a given directory.
     * @param directory the source code's directory
     *
     * @throws FileNotFoundException       the file not found exception
     * @throws ExceptionFromFile           the exception generated from a specific file
     */
    public static List<Throwable> compile(String directory) throws FileNotFoundException, ExceptionFromFile {
        init(directory);
        String[] sourceFiles = getFilenames(Generator.directory);

        // get classes from source files
        SyntaxTree sourceTree = getSourceSyntaxTree(sourceFiles);

        // generate methods and type symbols for the source code
        getTypesAndMethods(sourceTree);

        // compile code
        compile(sourceTree);

        // write code to files all have been compiled correctly
        writeToFiles();

        // output warnings
        return warnings;
    }

    private static void init(String directory) {
        Generator.directory = directory;
        symbolTables = new ArrayList<>();
        symbolTables.add(new SymbolTable(VariableSymbol.SegmentTypes.STATIC));
        symbolTables.add(new SymbolTable(VariableSymbol.SegmentTypes.ARGUMENT));
        symbolTables.add(new SymbolTable(VariableSymbol.SegmentTypes.LOCAL));
        symbolTableIndex = new HashMap<>();
        currentFile = "";
        currentReturnType = "void";
        currentClassname = "";
        currentLabel = "a";
        outputFiles = new HashMap<>();
        warnings = new ArrayList<>();
    }

    private static String[] getFilenames(String directory) throws FileNotFoundException {
        // check directory exists
        File f = new File(directory);
        if (!f.exists()) {
            throw new FileNotFoundException("No valid source directory supplied to compiler...");
        }

        // get files ending in .jack
        FilenameFilter filenameFilter = (dir, name) -> name.toLowerCase().endsWith(".jack");

        String[] filenames = f.list(filenameFilter);
        for (int i = 0; i < filenames.length; ++i) {
            filenames[i] = directory + "\\\\" + filenames[i];
        }

        // check there are files found
        if (filenames.length == 0) {
            throw new FileNotFoundException("No JACK files found in source directory...");
        }

        return filenames;
    }

    private static SyntaxTree getSourceSyntaxTree(String[] sourceFiles) throws ExceptionFromFile {
        SyntaxTree tree = new SyntaxTree(directory);

        // parse each file adding the classes to the syntax tree
        for (String filepath: sourceFiles) {
            try {
                Parser p = new Parser(filepath);
                SyntaxTree fileTree = p.parseFile();
                tree.addChild(fileTree);
            } catch (FileNotFoundException e) {
                // not possible, do nothing
            } catch (Throwable e) {
                // attach the source filename that cause the error to exception
                throw new ExceptionFromFile(e, filepath);
            }
        }

        return tree;
    }

    private static void getTypesAndMethods(SyntaxTree sourceTree) throws ExceptionFromFile {
        int symbolTableNumber = 3;

        for (SyntaxTreeNode fileTree: sourceTree.getChildren()) {
            try {
                for (SyntaxTreeNode classTree : fileTree.getChildren()) {
                    // add class name to type list
                    SymbolTable.TypeTable.insert(classTree);
                    String classname = classTree.getChild(0).toString();

                    // add variables to relative symbol tables
                    SymbolTable classFieldTable = new SymbolTable(VariableSymbol.SegmentTypes.THIS);
                    for (SyntaxTreeNode varNode : classTree.getChild(1).getChild(0).getChildren()) {
                        if (varNode.getChild(0).toString().equals("static")) {
                            symbolTables.get(STATIC_TABLE).insert(varNode, classname + ".");
                        } else {
                            classFieldTable.insert(varNode);
                        }
                    }

                    symbolTables.add(classFieldTable); // add symbol table for class
                    symbolTableIndex.put(classname, symbolTableNumber);
                    ++symbolTableNumber;

                    // add class methods to function list
                    for (SyntaxTreeNode functionNode : classTree.getChild(1).getChild(1).getChildren()) {
                        SymbolTable.FunctionTable.insert(functionNode, classname + ".");
                    }
                }
            } catch (Throwable e) {
                // attach the source filename that cause the error to exception
                throw new ExceptionFromFile(e, fileTree.toString());
            }
        }
    }

    private static void writeToFiles() throws ExceptionFromFile {
        for (Map.Entry<String, String> entry : outputFiles.entrySet()) {
            String filename = entry.getKey();
            String code = entry.getValue();

            try {
                BufferedWriter writer = new BufferedWriter(new FileWriter(filename));
                writer.write(code);
                writer.close();
            } catch (IOException e) {
                throw new ExceptionFromFile(new Exception("Unknown IO exception writing to file"), filename);
            }
        }
    }

    private static void compile(SyntaxTree sourceTree) throws ExceptionFromFile {
        String outputFile = "";

        // compile code from classes
        for (SyntaxTreeNode fileTree: sourceTree.getChildren()) {
            currentFile = fileTree.toString();

            for (SyntaxTreeNode classTree: fileTree.getChildren()) {
                // get current class name and output filepath
                currentClassname = classTree.getChild(0).toString();
                outputFile = String.format("%s\\\\%s.vm", directory, currentClassname);

                // compile the class
                StringBuilder code = new StringBuilder();
                for (SyntaxTreeNode routineNode : classTree.getChild(1).getChild(1).getChildren()) {
                    code.append(compileRoutine(routineNode));
                }

                // add to output files dictionary
                outputFiles.put(outputFile, code.toString());
            }
        }
    }

    private static String compileRoutine(SyntaxTreeNode routineNode) throws ExceptionFromFile {
        // clear symbol tables
        symbolTables.get(ARGUMENT_TABLE).clear();
        symbolTables.get(LOCAL_TABLE).clear();

        // generate this argument
        SyntaxTreeNode thisParam = new SyntaxTreeNode(SyntaxTreeNode.SyntaxTypes.PARAM);
        thisParam.addChild(new SyntaxTreeTerminal(new Token(currentClassname, Token.TokenTypes.IDENTIFIER, routineNode.getLine())));
        thisParam.addChild(new SyntaxTreeTerminal(new Token("this", Token.TokenTypes.IDENTIFIER, routineNode.getLine())));

        // add this argument for methods
        if (routineNode.getChild(0).toString().equals("method")) {
            try {
                symbolTables.get(ARGUMENT_TABLE).insert(thisParam);
            } catch (Throwable e) {
                catchException(e);
            }
        }

        // populate remainder of argument table
        for (SyntaxTreeNode paramNode: routineNode.getChild(3).getChildren()) {
            // verify variable is not in other segment in scope
            if (getLocalSymbol(paramNode.getChild(1).toString()) != null) {
                catchException(new ExistingSymbolException(((SyntaxTreeTerminal) paramNode.getChild(1)).getToken(), "variable"));
            }

            try {
                symbolTables.get(ARGUMENT_TABLE).insert(paramNode);
            } catch (Throwable e) {
                catchException(e);
            }
        }

        // add this argument for constructors
        if (routineNode.getChild(0).toString().equals("constructor")) {
            try {
                symbolTables.get(ARGUMENT_TABLE).insert(thisParam);
            } catch (Throwable e) {
                catchException(e);
            }
        }

        // compile function code
        currentReturnType = routineNode.getChild(1).toString();
        CodeBlock functionBlock = compileCodeBlock(routineNode.getChild(4));
        if (!functionBlock.isReturned()) {
            // verify the routine returned
            catchException(new UnreturnedFunctionException(currentClassname, routineNode));
        }

        // insert function name at beginning of code block
        StringBuilder builder = new StringBuilder();
        builder.append(String.format("function %s.%s %d\n", currentClassname,
                routineNode.getChild(2).toString(), symbolTables.get(LOCAL_TABLE).size()));

        if (routineNode.getChild(0).toString().equals("constructor")) {
            // check if needing to allocate memory (constructor)
            builder.append(String.format("push constant %d\ncall Memory.alloc 1\npop pointer 0\n",
                    SymbolTable.TypeTable.getSymbol(routineNode.getChild(1).toString()).getSize()));
        } else if (routineNode.getChild(0).toString().equals("method")) {
            // check if need to set this pointer
            builder.append("push argument 0\npop pointer 0\n");
        }

        // add code and return
        builder.append(functionBlock.getCode());
        return builder.toString();
    }

    private static CodeBlock compileCodeBlock(SyntaxTreeNode blockBodyNode) throws ExceptionFromFile {
        StringBuilder builder = new StringBuilder();
        boolean returned = false;

        // compile statements
        for (SyntaxTreeNode statementNode: blockBodyNode.getChildren()) {
            if (returned) {
                // mode code after a return is unreachable
                catchException(new UnreachableCodeException(statementNode.getLine()));
            }

            CodeBlock statementBlock = compileStatement(statementNode);
            builder.append(String.format("%s", statementBlock.getCode()));
            returned = statementBlock.isReturned();
        }

        return new CodeBlock(builder.toString(), returned);
    }

    private static CodeBlock compileStatement(SyntaxTreeNode statementNode) throws ExceptionFromFile {
        switch (statementNode.getType()) {
            case ROUTINE_VAR:
                return new CodeBlock(compileVarDeclare(statementNode), false);
            case LET:
                return new CodeBlock(compileLetStatement(statementNode), false);
            case IF:
                return compileIfStatement(statementNode);
            case WHILE:
                return compileWhileStatement(statementNode);
            case DO:
                return new CodeBlock(compileRoutineCall(statementNode) + "pop temp 0\n", false);
            case RETURN:
                return new CodeBlock(compileReturnStatement(statementNode), true);
        }

        // not reachable, but required to avoid syntax error
        return new CodeBlock("", false);
    }

    private static String compileVarDeclare(SyntaxTreeNode statementNode) throws ExceptionFromFile {
        // verify variable is not in other segment in scope
        for (int i = 1; i < statementNode.countChildren(); ++i) {
            if (getLocalSymbol(statementNode.getChild(i).toString()) != null) {
                catchException(new ExistingSymbolException(((SyntaxTreeTerminal) statementNode.getChild(i)).getToken(), "variable"));
            }
        }

        try {
            symbolTables.get(LOCAL_TABLE).insert(statementNode);
        } catch (Throwable e) {
            catchException(e);
        }

        return "";
    }

    private static String compileLetStatement(SyntaxTreeNode statementNode) throws ExceptionFromFile {
        StringBuilder builder = new StringBuilder();

        // get variable symbol
        VariableSymbol var = getLocalSymbol(statementNode.getChild(0).toString());
        if (var == null) {
            catchException(new UnrecognisedSymbolException(((SyntaxTreeTerminal) statementNode.getChild(0)).getToken(), "variable"));
        }

        // handle arrays being accessed
        if (statementNode.countChildren() == 3) {
            // compile expression to temp 0
            builder.append(compileExpression(statementNode.getChild(2), ""));
            builder.append("pop temp 0\n");

            // access the array
            builder.append(compileArrayAccess("", statementNode.getChild(0).toString(), statementNode.getChild(1), false));

            // put expression value into array
            builder.append("push temp 0\npop that 0\n");
        } else {
            // compile expression and pop to variable
            builder.append(compileExpression(statementNode.getChild(1), var.getType()));
            builder.append(String.format("pop %s %d\n", var.getSegment(), var.getOffset()));

            var.setToInitialised(); // set variable to initialised
        }

        return builder.toString();
    }

    private static CodeBlock compileIfStatement(SyntaxTreeNode statementNode) throws ExceptionFromFile {
        StringBuilder builder = new StringBuilder();

        // compile compliment of expression
        builder.append(compileExpression(statementNode.getChild(0), "boolean"));
        builder.append("not\n");

        // get labels for program flow
        String label1 = getNextLabel();
        String label2 = getNextLabel();

        // compile if and main clause body
        builder.append(String.format("if-goto %s\n", label1));
        CodeBlock block = compileCodeBlock(statementNode.getChild(1));
        boolean returned = block.isReturned(); // used to see if both branches returned
        builder.append(block.getCode());

        // compile program flow labels
        builder.append(String.format("goto %s\nlabel %s\n", label2, label1));

        // compile else clause body
        if (statementNode.countChildren() == 3) {
            block = compileCodeBlock(statementNode.getChild(2));
            returned &= block.isReturned();
            builder.append(block.getCode());
        }

        // end label
        builder.append(String.format("label %s\n", label2));

        return new CodeBlock(builder.toString(), returned);
    }

    private static CodeBlock compileWhileStatement(SyntaxTreeNode statementNode) throws ExceptionFromFile {
        StringBuilder builder = new StringBuilder();

        // get labels for program flow
        String label1 = getNextLabel();
        String label2 = getNextLabel();
        builder.append(String.format("label %s\n", label1));

        // compile compliment of expression and program flow
        builder.append(compileExpression(statementNode.getChild(0), "boolean"));
        builder.append(String.format("not\nif-goto %s\n", label2));

        // compile while body
        CodeBlock block = compileCodeBlock(statementNode.getChild(1));
        builder.append(block.getCode());

        // compile program flow labels
        builder.append(String.format("goto %s\nlabel %s\n", label1, label2));

        return new CodeBlock(builder.toString(), block.isReturned());
    }

    private static String compileRoutineCall(SyntaxTreeNode statementNode, String wantedTypes) throws ExceptionFromFile {
        StringBuilder builder = new StringBuilder();

        // get method call classname
        String classname = currentClassname;
        VariableSymbol var = null;
        boolean function = false;
        int children = statementNode.countChildren();
        if (children == 3) {
            var = getLocalSymbol(statementNode.getChild(0).toString());
            if (var == null) {
                // no local symbol found, try check for type
                if (!SymbolTable.TypeTable.lookup(statementNode.getChild(0).toString())) {
                    catchException(new UnrecognisedSymbolException(((SyntaxTreeTerminal) statementNode.getChild(0)).getToken(), "identifier or type"));
                }

                classname = statementNode.getChild(0).toString();
                function = true;
            } else {
                classname = var.getType();

                // check initialised
                if (var.getSegment() == VariableSymbol.SegmentTypes.LOCAL && !var.isInitialised()) {
                    catchException(new UninitializedVariableException(new Token(statementNode.getChild(0).toString(), Token.TokenTypes.IDENTIFIER, statementNode.getLine())));
                }
            }
        }

        // search for method/function
        String functionName = statementNode.getChild(children - 2).toString();
        FunctionSymbol functionSymbol;
        if (function) {
            functionSymbol = getFunctionSymbol(functionName, classname);
        } else {
            functionSymbol = getMethodSymbol(functionName, classname);
            if (functionSymbol == null) {
                // incase it was a function inside own class
                functionSymbol = getFunctionSymbol(functionName, classname);
                function = true;
            } else {
                // push this parameter
                if (var == null) {
                    builder.append("push pointer 0\n");
                } else {
                    builder.append(String.format("push %s %s\n", var.getSegment(), var.getOffset()));
                }
            }
        }

        // check if function was found and returns wanted type
        if (functionSymbol == null) {
            catchException(new UnrecognisedSymbolException(((SyntaxTreeTerminal) statementNode.getChild(children - 2)).getToken(), "function"));
        } else if (!wantedTypes.equals("") && !isValidType(functionSymbol.getReturnType(), wantedTypes)) {
            catchException(new IncompatibleTypeException(statementNode.getLine(), ((SyntaxTreeTerminal) statementNode.getChild(children - 2)).getToken(), functionSymbol.getReturnType(), wantedTypes));
        }

        // check argument count is same
        if (functionSymbol.getArgsCount() != statementNode.getChild(children - 1).countChildren()) {
            catchException(new InvalidArgumentsException(statementNode.getLine(), statementNode.getChild(children - 1).countChildren(), functionSymbol.getArgsCount(), functionName));
        }

        // push parameters
        for (int i = 0; i < functionSymbol.getArgsCount(); ++i) {
            builder.append(compileExpression(statementNode.getChild(children - 1).getChild(i), functionSymbol.getArgTypes()[i]));
        }

        // call statement
        builder.append(String.format("call %s.%s %d\n", classname, functionName, functionSymbol.getArgsCount() + (functionSymbol.isFunction() ? 0 : 1)));

        return builder.toString();
    }

    private static String compileRoutineCall(SyntaxTreeNode statementNode) throws ExceptionFromFile {
        return compileRoutineCall(statementNode, "");
    }

    private static String compileReturnStatement(SyntaxTreeNode statementNode) throws ExceptionFromFile {
        // try get expression to return
        String expression = "";
        if (statementNode.countChildren() > 0) {
            expression = compileExpression(statementNode.getChild(0), currentReturnType);
        } else if (!currentReturnType.equals("void")) {
            catchException(new IncompatibleTypeException(statementNode.getLine(), new Token("return", Token.TokenTypes.KEYWORD_STATEMENT, statementNode.getLine()), "void", currentReturnType));
        }

        // compile return statement
        return String.format("%sreturn\n", expression);
    }

    private static String compileExpression(SyntaxTreeNode expressionNode, String wantedTypes) throws ExceptionFromFile {
        // deal with being called with parent node
        if (expressionNode.getType() == SyntaxTreeNode.SyntaxTypes.EXPRESSION) {
            return compileExpression(expressionNode.getChild(0), wantedTypes);
        }

        if ("&|".contains(expressionNode.toString())) {
            // boolean operations
            if (!isValidType("int boolean", wantedTypes)) {
                catchException(new IncompatibleTypeException(expressionNode.getLine(),
                        ((SyntaxTreeOperationNode) expressionNode).getToken(), "boolean", wantedTypes));
            }

            return compileBoolean(expressionNode);
        } else if ("=<>".contains(expressionNode.toString())) {
            // comparison
            if (!isValidType("boolean", wantedTypes)) {
                catchException(new IncompatibleTypeException(expressionNode.getLine(),
                        ((SyntaxTreeOperationNode) expressionNode).getToken(), "boolean", wantedTypes));
            }

            return compileComparison(expressionNode);
        } else if ("+-".contains(expressionNode.toString()) && expressionNode.countChildren() == 2) {
            // addition or subtraction
            if (!isValidType("int", wantedTypes)) {
                catchException(new IncompatibleTypeException(expressionNode.getLine(),
                        ((SyntaxTreeOperationNode) expressionNode).getToken(), "int", wantedTypes));
            }

            return compileArithmetic(expressionNode);
        } else if ("*/".contains(expressionNode.toString())) {
            // multiply or divide
            if (!isValidType("int", wantedTypes)) {
                catchException(new IncompatibleTypeException(expressionNode.getLine(),
                        ((SyntaxTreeOperationNode) expressionNode).getToken(), "int", wantedTypes));
            }

            return compileTerm(expressionNode);
        } else if (expressionNode.toString().equals("-")) {
            // unary minus
            if (!isValidType("int", wantedTypes)) {
                catchException(new IncompatibleTypeException(expressionNode.getLine(),
                        ((SyntaxTreeOperationNode) expressionNode).getToken(), "int", wantedTypes));
            }

            return String.format("%sneg\n", compileExpression(expressionNode.getChild(0), "int"));
        } else if (expressionNode.toString().equals("~")) {
            // boolean negation
            if (!isValidType("int boolean", wantedTypes)) {
                catchException(new IncompatibleTypeException(expressionNode.getLine(),
                        ((SyntaxTreeOperationNode) expressionNode).getToken(), "boolean", wantedTypes));
            }

            return String.format("%snot\n", compileExpression(expressionNode.getChild(0), "boolean"));
        } else if (expressionNode.getType() == SyntaxTreeNode.SyntaxTypes.OPERAND_DATA) {
            // routine call, array access, or class member access
            return compileComplexExpressionTerm(expressionNode, wantedTypes);
        } else {
            // not operator, must compile expression value
            return compileExpressionTerm((SyntaxTreeTerminal) expressionNode, wantedTypes);
        }
    }

    private static String compileBoolean(SyntaxTreeNode expressionNode) throws ExceptionFromFile {
        return compileExpression(expressionNode.getChild(0), "boolean int") +
                compileExpression(expressionNode.getChild(1), "boolean int") +
                String.format("%s\n", expressionNode.toString().equals("&") ? "and" : "or");
    }

    private static String compileComparison(SyntaxTreeNode expressionNode) throws ExceptionFromFile {
        StringBuilder builder = new StringBuilder();

        builder.append(compileExpression(expressionNode.getChild(0), ""));
        builder.append(compileExpression(expressionNode.getChild(1), ""));

        switch (expressionNode.toString()) {
            case "=":
                builder.append("eq\n");
                break;
            case ">":
                builder.append("gt\n");
                break;
            case "<":
                builder.append("lt\n");
                break;
        }

        return builder.toString();
    }

    private static String compileArithmetic(SyntaxTreeNode expressionNode) throws ExceptionFromFile {
        StringBuilder builder = new StringBuilder();

        builder.append(compileExpression(expressionNode.getChild(0), "int"));
        builder.append(compileExpression(expressionNode.getChild(1), "int"));

        switch (expressionNode.toString()) {
            case "+":
                builder.append("add\n");
                break;
            case "-":
                builder.append("sub\n");
                break;
        }

        return builder.toString();
    }

    private static String compileTerm(SyntaxTreeNode expressionNode) throws ExceptionFromFile {
        StringBuilder builder = new StringBuilder();

        builder.append(compileExpression(expressionNode.getChild(0), "int"));
        builder.append(compileExpression(expressionNode.getChild(1), "int"));

        switch (expressionNode.toString()) {
            case "*":
                builder.append("call Math.multiply 2\n");
                break;
            case "/":
                builder.append("call Math.divide 2\n");
                break;
        }

        return builder.toString();
    }

    private static String compileComplexExpressionTerm(SyntaxTreeNode expressionNode, String wantedTypes) throws ExceptionFromFile {
        // check if function call
        if (expressionNode.getChild(expressionNode.countChildren() - 1).getType() == SyntaxTreeNode.SyntaxTypes.EXPRESSION_LIST) {
            return compileRoutineCall(expressionNode, wantedTypes);
        }

        // check for array usage
        if (expressionNode.getChild(expressionNode.countChildren() - 1).getType() == SyntaxTreeNode.SyntaxTypes.EXPRESSION) {
            if (expressionNode.countChildren() == 3) {
                // other class's array
                return compileArrayAccess(expressionNode.getChild(0).toString(),
                        expressionNode.getChild(1).toString(), expressionNode.getChild(2), true) + "push that 0\n";
            } else {
                // local array
                return compileArrayAccess("", expressionNode.getChild(0).toString(),
                        expressionNode.getChild(1), true) + "push that 0\n";
            }
        }

        // neither, must be class member access
        VariableSymbol[] data = tryGetClassSymbols(expressionNode.getChild(0).toString(), expressionNode.getChild(1).toString(), expressionNode.getLine(), true);
        VariableSymbol thatSymbol = data[0];
        VariableSymbol accessSymbol = data[1];
        return String.format("%spush that %d\n", compileThatAccess(thatSymbol), accessSymbol.getOffset());
    }

    private static String compileArrayAccess(String ownerClass, String identifier, SyntaxTreeNode expressionNode, boolean initRequired) throws ExceptionFromFile {
        // get symbol identifiers
        VariableSymbol[] data = tryGetClassSymbols(ownerClass, identifier, expressionNode.getLine(), initRequired);
        VariableSymbol ownerSymbol = data[0];
        VariableSymbol arraySymbol = data[1];

        // access object if there was owner symbol
        String ownerString = "";
        if (ownerSymbol != null) {
            ownerString = compileThatAccess(ownerSymbol);
        }

        // access array location
        return String.format("push %s %d\n%s%sadd\npop pointer 1\n",
            arraySymbol.getSegment() == VariableSymbol.SegmentTypes.THIS && ownerSymbol != null ? "that" : arraySymbol.getSegment(),
            arraySymbol.getOffset(), ownerString, compileExpression(expressionNode, "int"));
    }

    private static String compileThatAccess(VariableSymbol thatSymbol) {
        return String.format("push %s %d\npop pointer 1\n", thatSymbol.getSegment(), thatSymbol.getOffset());
    }

    private static String compileExpressionTerm(SyntaxTreeTerminal expressionNode, String wantedTypes) throws ExceptionFromFile {
        Token valueToken = expressionNode.getToken();

        // compile for default types and keywords
        switch (valueToken.getType()) {
            case NUMBER:
                if (!isValidType("int", wantedTypes)) {
                    catchException(new IncompatibleTypeException(expressionNode.getLine(), expressionNode.getToken(), "int", wantedTypes));
                }

                return String.format("push constant %s\n", valueToken.getLexeme());
            case KEYWORD_BOOLEAN:
                if (!isValidType("boolean", wantedTypes)) {
                    catchException(new IncompatibleTypeException(expressionNode.getLine(), expressionNode.getToken(), "boolean", wantedTypes));
                }

                if (valueToken.getLexeme().equals("true")) {
                    return "push constant 1\nnot\n";
                } else {
                    return "push constant 0\n";
                }
            case KEYWORD_NULL:
                return "push constant 0\n";
            case STRING:
                if (!isValidType("String", wantedTypes)) {
                    catchException(new IncompatibleTypeException(expressionNode.getLine(), expressionNode.getToken(), "String", wantedTypes));
                }

                return compileString(valueToken);
        }

        // not default type, compile identifier
        if (valueToken.getLexeme().equals("this")) {
            return "push pointer 0\n";
        } else {
            VariableSymbol symbol = getLocalSymbol(valueToken.getLexeme());
            if (!isValidType(symbol.getType(), wantedTypes)) {
                catchException(new IncompatibleTypeException(expressionNode.getLine(), expressionNode.getToken(), symbol.getType(), wantedTypes));
            }

            return String.format("push %s %d\n", symbol.getSegment(), symbol.getOffset());
        }
    }

    private static String compileString(Token stringToken) {
        StringBuilder builder = new StringBuilder();

        // get string to push to stack
        String str = stringToken.getLexeme();
        str = str.substring(1, str.length() - 1);

        // create string object on stack
        builder.append(String.format("push constant %d\ncall String.new 1\n", str.length()));

        for (int i = 0 ; i < str.length(); ++i) {
            builder.append(String.format("push constant %d\ncall String.appendChar 2\n", ((int) str.charAt(i))));
        }

        return builder.toString();
    }

    public static boolean isValidType(String gottenTypes, String expectedTypes) {
        // no type expected
        if (expectedTypes.equals("")) {
            return true;
        }

        List<String> gottenTypeList = Arrays.asList(gottenTypes.split(" "));
        List<String> expectedTypeList = Arrays.asList(expectedTypes.split(" "));

        // check type matches
        for (String gottenType: gottenTypeList) {
            if (expectedTypeList.contains(gottenType)) {
                return true;
            }
        }

        // not compatible types
        return false;
    }

    private static VariableSymbol getLocalSymbol(String varName) {
        // check argument table
        VariableSymbol getAttempt = symbolTables.get(ARGUMENT_TABLE).getSymbol(varName);
        if (getAttempt != null) {
            return getAttempt;
        }

        // check symbol table
        getAttempt = symbolTables.get(LOCAL_TABLE).getSymbol(varName);
        if (getAttempt != null) {
            return getAttempt;
        }

        // check own symbol table
        getAttempt = symbolTables.get(ARGUMENT_TABLE).getSymbol("this");
        if (getAttempt != null) {
            getAttempt = symbolTables.get(symbolTableIndex.get(getAttempt.getType())).getSymbol(varName);
            if (getAttempt != null) {
                return getAttempt;
            }
        }

        // check static table
        getAttempt = symbolTables.get(STATIC_TABLE).getSymbol(currentClassname + "." + varName);
        return getAttempt; // will be null if no result found
    }

    private static VariableSymbol getSymbol(String varName, String classname) {
        // check symbol table for class
        VariableSymbol getAttempt = symbolTables.get(symbolTableIndex.get(classname)).getSymbol(varName);
        if (getAttempt != null) {
            return getAttempt;
        }

        // check static symbol table
        getAttempt = symbolTables.get(STATIC_TABLE).getSymbol(classname + "." + varName);
        return getAttempt; // will be null if no result found
    }

    private static VariableSymbol getSymbol(String varName, VariableSymbol object) {
        return getSymbol(varName, object.getType());
    }

    private static VariableSymbol[] tryGetClassSymbols(String classname, String identifier, int line, boolean initRequired) throws ExceptionFromFile {
        VariableSymbol symbol, ownerSymbol = null;
        if (!classname.equals("")) {
            // get symbol of class
            ownerSymbol = getLocalSymbol(classname);

            if (ownerSymbol == null) {
                // not found, try get as a static class symbol
                symbol = getSymbol(identifier, classname);
            } else {
                // verify symbol is initialised
                if (ownerSymbol.getSegment() == VariableSymbol.SegmentTypes.LOCAL && !ownerSymbol.isInitialised()) {
                    catchException(new UninitializedVariableException(new Token(classname, Token.TokenTypes.IDENTIFIER, line)));
                    return null; // to avoid compiler error, will not reach here since catchException will block since its not a warning
                } else {
                    symbol = getSymbol(identifier, ownerSymbol);
                }
            }
        } else {
            symbol = getLocalSymbol(identifier);
        }

        // check if valid symbol
        if (symbol == null) {
            catchException(new UnrecognisedSymbolException(line,
                    (classname.equals("") ? classname + "." : "") + identifier, "variable"));
        }

        // check if initialised
        if (symbol.getSegment() == VariableSymbol.SegmentTypes.LOCAL && !symbol.getType().equals("Array") && initRequired && !symbol.isInitialised()) {
            catchException(new UninitializedVariableException(new Token(classname + "." + identifier, Token.TokenTypes.IDENTIFIER, line)));
        }

        VariableSymbol[] data = new VariableSymbol[2];
        data[0] = ownerSymbol;
        data[1] = symbol;
        return data;
    }

    private static FunctionSymbol getMethodSymbol(String methodName, String classname) {
        // check method table
        return SymbolTable.FunctionTable.getMethodSymbol(classname + "." + methodName);
    }

    private static FunctionSymbol getFunctionSymbol(String functionName, String classname) {
        // check function table
        return SymbolTable.FunctionTable.getFunctionSymbol(classname + "." + functionName);
    }

    private static String getNextLabel() {
        // get next label
        String label = currentLabel;

        // increment to next label
        for (int i = currentLabel.length() - 1; i >= 0; i--) {
            // get current letter, string before, and after
            char c = currentLabel.charAt(i);
            String prefix = currentLabel.substring(0, i);
            String suffix = currentLabel.substring(i + 1);

            // try increment last char
            if (c == 'z') {
                currentLabel = prefix + 'a' + suffix;
                if (i == 0) {
                    currentLabel += 'a'; // if cycled whole string, add a new character
                }
            } else {
                currentLabel = prefix + ((char) (c + 1)) + suffix;
                break;
            }
        }

        return label;
    }

    private static void catchException(Throwable e) throws ExceptionFromFile {
        // check if it is an incompatible type exception, then set to warning
        if (!strictTypes && e instanceof IncompatibleTypeException) {
            warnings.add(new ExceptionFromFile(e, currentFile));
        } else {
            throw new ExceptionFromFile(e, currentFile);
        }
    }
}
