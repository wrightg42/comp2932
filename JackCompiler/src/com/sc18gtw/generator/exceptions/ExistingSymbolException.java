package com.sc18gtw.generator.exceptions;

import com.sc18gtw.lexer.Token;

/**
 * An exception for when existing symbol's are redefined at the semantic analysis stage.
 */
public class ExistingSymbolException extends Throwable {
    /**
     * Instantiates a new Existing symbol exception.
     *
     * @param symbol     the symbol
     * @param symbolKind the symbol kind
     */
    public ExistingSymbolException(Token symbol, String symbolKind) {
        super(String.format("Line %d: %s \"%s\" is already defined",
                symbol.getLine(), symbolKind, symbol.getLexeme()));

        this.symbol = symbol;
        this.symbolKind = symbolKind;
    }

    private Token symbol;
    private String symbolKind;
}
