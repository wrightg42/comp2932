package com.sc18gtw.generator.exceptions;

import com.sc18gtw.lexer.Token;

/**
 * An exception for unrecognised symbol's at the semantic analysis stage.
 */
public class UnrecognisedSymbolException extends Throwable {
    /**
     * Instantiates a new Unrecognised symbol exception.
     *
     * @param symbol     the symbol
     * @param symbolKind the symbol kind
     */
    public UnrecognisedSymbolException(Token symbol, String symbolKind) {
        super(String.format("Line %d: \"%s\" is an unrecognised %s",
                symbol.getLine(), symbol.getLexeme(), symbolKind));
        this.symbol = symbol;
        this.symbolKind = symbolKind;
    }

    /**
     * Instantiates a new Unrecognised symbol exception.
     *
     * @param line       the line
     * @param symbol     the symbol name
     * @param symbolKind the symbol kind
     */
    public UnrecognisedSymbolException(int line, String symbol, String symbolKind) {
        this(new Token(symbol, Token.TokenTypes.IDENTIFIER, line), symbolKind);
    }

    private Token symbol;
    private String symbolKind;
}
