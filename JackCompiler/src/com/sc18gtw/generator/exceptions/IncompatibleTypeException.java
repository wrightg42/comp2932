package com.sc18gtw.generator.exceptions;

import com.sc18gtw.lexer.Token;

/**
 * The type Incompatible type exception.
 */
public class IncompatibleTypeException extends Throwable {
    /**
     * Instantiates a new Incompatible type exception.
     *
     * @param line         the line
     * @param nearToken    the token nearby which caused the exception
     * @param gottenType   the gotten type
     * @param expectedType the expected type
     */
    public IncompatibleTypeException(int line, Token nearToken, String gottenType, String expectedType) {
        super(String.format("Line %d, near \"%s\": Expected expression of type \"%s\" got expression of type \"%s\"",
                line, nearToken.getLexeme(), expectedType, gottenType));

        this.line = line;
        this.gottenType = gottenType;
        this.expectedType = expectedType;
        this.nearToken = nearToken;
    }

    private int line;
    private String gottenType;
    private String expectedType;
    private Token nearToken;
}
