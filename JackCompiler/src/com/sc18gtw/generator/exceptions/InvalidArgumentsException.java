package com.sc18gtw.generator.exceptions;

import com.sc18gtw.lexer.Token;

/**
 * The type Invalid arguments exception.
 */
public class InvalidArgumentsException extends Throwable {
    /**
     * Instantiates a new Invalid arguments exception.
     *
     * @param line       the line
     * @param gotArgs    the got args
     * @param wantedArgs the wanted args
     * @param functionName the function name which call created this exception
     */
    public InvalidArgumentsException(int line, int gotArgs, int wantedArgs, String functionName) {
        super(String.format("Line %d, near \"%s\": Expected %d arguments, got %d",
                line, functionName, gotArgs, wantedArgs));

        this.line = line;
        this.gotArgs = gotArgs;
        this.wantedArgs = wantedArgs;
        this.functionName = functionName;
    }

    private int line;
    private int gotArgs;
    private int wantedArgs;
    private String functionName;
}
