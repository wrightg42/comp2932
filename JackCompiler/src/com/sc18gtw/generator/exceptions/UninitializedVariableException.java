package com.sc18gtw.generator.exceptions;

import com.sc18gtw.lexer.Token;

/**
 * The type Uninitialized variable exception.
 */
public class UninitializedVariableException extends Throwable {
    /**
     * Instantiates a new Uninitialized variable exception.
     *
     * @param symbol the symbol that was not initialized
     */
    public UninitializedVariableException(Token symbol) {
        super(String.format("Line %d: Variable \"%s\" used before initialization", symbol.getLine(), symbol.getLexeme()));

        uninitializedSymbol = symbol;
    }

    private Token uninitializedSymbol;
}
