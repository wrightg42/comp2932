package com.sc18gtw.generator.exceptions;

/**
 * An exception for unreachable code at the semantic analysis stage.
 */
public class UnreachableCodeException extends Throwable {
    /**
     * Instantiates a new Unreachable code exception.
     *
     * @param line the line that unreachable code was found
     */
    public UnreachableCodeException(int line) {
        super(String.format("Line %d: Unreachable code after return statement", line));

        unreachableLine = line;
    }

    private int unreachableLine;
}
