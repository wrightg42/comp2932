package com.sc18gtw.generator.exceptions;

import com.sc18gtw.parser.syntaxtree.SyntaxTreeNode;
import com.sc18gtw.parser.syntaxtree.SyntaxTreeTerminal;

/**
 * An exception for unreturned function's at the semantic analysis stage.
 */
public class UnreturnedFunctionException extends Throwable {
    /**
     * Instantiates a new Unreturned function exception.
     *
     * @param classname   the classname
     * @param routineNode the routine node
     */
    public  UnreturnedFunctionException(String classname, SyntaxTreeNode routineNode) {
        super(String.format("Line %d: Not all code in %s.%s returns a value", ((SyntaxTreeTerminal) routineNode.getChild(0)).getToken().getLine(),
                classname, routineNode.getChild(2).toString()));

        this.classname = classname;
        routineName = routineNode.getChild(2).toString();
        routineLine = ((SyntaxTreeTerminal) routineNode.getChild(0)).getToken().getLine();
    }

    private String classname;
    private String routineName;
    private int routineLine;
}
