package com.sc18gtw.exceptions;

/**
 * An exception for showing which file the exception was raised.
 */
public class ExceptionFromFile extends Throwable {

    /**
     * Instantiates a new Exception from file.
     *
     * @param e        the exception
     * @param filename the file the exception was raised
     */
    public ExceptionFromFile(Throwable e, String filename) {
        super(String.format("In file \"%s\", %s", filename.split("\\\\")[filename.split("\\\\").length - 1], e.getMessage()));

        this.filename = filename;
        exception = e;
    }

    private String filename;
    private Throwable exception;
}
