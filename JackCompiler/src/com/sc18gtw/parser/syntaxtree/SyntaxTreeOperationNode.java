package com.sc18gtw.parser.syntaxtree;

import com.sc18gtw.lexer.Token;

/**
 * Represents an operation node in the Syntax tree.
 */
public class SyntaxTreeOperationNode extends SyntaxTreeNode {
    /**
     * Instantiates a new Syntax tree operation node.
     *
     * @param token the underlying token of the terminal
     */
    public SyntaxTreeOperationNode(Token token) {
        super(SyntaxTreeNode.SyntaxTypes.OPERATION);
        this.token = token;
        line = token.getLine();
    }

    private Token token;

    /**
     * Gets the underlying token.
     *
     * @return the token
     */
    public Token getToken() {
        return token;
    }

    @Override
    public String toString() {
        return token.getLexeme();
    }

    @Override
    public int getLine() {
        return token.getLine();
    }
}
