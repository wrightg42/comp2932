package com.sc18gtw.parser.syntaxtree;

/**
 * Represents a Syntax tree.
 */
public class SyntaxTree extends SyntaxTreeNode {
    /**
     * Instantiates a new Syntax tree.
     *
     * @param filename the filename of the source file the syntax tree represents.
     */
    public SyntaxTree(String filename) {
        super(SyntaxTypes.FILE);
        this.filename = filename;
    }

    private String filename;

    /**
     * Gets filename this syntax tree came from.
     *
     * @return the filename
     */
    public String getFilename() {
        return filename;
    }

    @Override
    public String toString() {
        return getFilename();
    }

    @Override
    public int getLine() {
        return 0;
    }
}
