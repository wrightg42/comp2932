package com.sc18gtw.parser.syntaxtree;

import java.util.*;

/**
 * Represents a node in the Syntax tree.
 */
public class SyntaxTreeNode {
    /**
     * The enum for different syntax node types.
     */
    public enum SyntaxTypes {
        /**
         * File syntax node type.
         */
        FILE,
        /**
         * Class syntax node type.
         */
        CLASS,
        /**
         * Class body syntax node type.
         */
        CLASS_BODY,
        /**
         * Collection of class variable declarations syntax node type.
         */
        CLASS_VARS,
        /**
         * Class variable declaration syntax node type.
         */
        CLASS_VAR,
        /**
         * Collection of class routine declarations syntax node type.
         */
        CLASS_ROUTINES,
        /**
         * Class routine declaration syntax node types.
         */
        CLASS_ROUTINE,
        /**
         * Parameter list syntax node types.
         */
        PARAMS_LIST,
        /**
         * Parameter syntax node types.
         */
        PARAM,
        /**
         * Routine body syntax node types.
         */
        ROUTINE_BODY,
        /**
         * Routine variable declaration syntax node types.
         */
        ROUTINE_VAR,
        /**
         * Let statement syntax node types.
         */
        LET,
        /**
         * If statement syntax node types.
         */
        IF,
        /**
         * While loop syntax node types.
         */
        WHILE,
        /**
         * Do statement syntax node types.
         */
        DO,
        /**
         * Return statement syntax node types.
         */
        RETURN,
        /**
         * Expression list syntax node types.
         */
        EXPRESSION_LIST,
        /**
         * Expression syntax node types.
         */
        EXPRESSION,
        /**
         * Operation syntax node types.
         */
        OPERATION,
        /**
         * Operand with data syntax node type.
         */
        OPERAND_DATA,
        /**
         * Terminal syntax node types.
         */
        TERMINAL
    }

    /**
     * Instantiates a new Syntax tree node.
     *
     * @param type the type of statement this node represents
     */
    public SyntaxTreeNode(SyntaxTypes type) {
        this.type = type;
        children = new LinkedList<>();
        line = 0;
    }

    private SyntaxTypes type;
    private List<SyntaxTreeNode> children;
    protected int line;

    /**
     * Gets the type of the node.
     *
     * @return the type
     */
    public SyntaxTypes getType() {
        return type;
    }

    /**
     * Gets the children of the node.
     *
     * @return the children
     */
    public List<SyntaxTreeNode> getChildren() {
        return children;
    }

    public SyntaxTreeNode getChild(int index) {
        return getChildren().get(index);
    }

    /**
     * Counts the children of the node.
     *
     * @return the amount of children
     */
    public int countChildren() {
        return children.size();
    }

    /**
     * Add a child to the node.
     *
     * @param node the node
     */
    public void addChild(SyntaxTreeNode node) {
        children.add(node);
        if (children.size() == 1) {
            line = node.getLine();
        }
    }

    /**
     * Gets the current line number of the syntax tree node.
     *
     * @return the line number
     */
    public int getLine() {
        return line;
    }

    @Override
    public String toString() {
        return type.name();
    }
}
