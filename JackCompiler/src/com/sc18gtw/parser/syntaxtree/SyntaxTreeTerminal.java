package com.sc18gtw.parser.syntaxtree;

import com.sc18gtw.lexer.Token;

/**
 * Represents a terminal node in the Syntax tree.
 */
public class SyntaxTreeTerminal extends SyntaxTreeNode {
    /**
     * Instantiates a new Syntax tree terminal node.
     *
     * @param token the underlying token of the terminal
     */
    public SyntaxTreeTerminal(Token token) {
        super(SyntaxTypes.TERMINAL);
        this.token = token;
        line = token.getLine();
    }

    private Token token;

    /**
     * Gets the underlying token.
     *
     * @return the token
     */
    public Token getToken() {
        return token;
    }

    @Override
    public void addChild(SyntaxTreeNode node) {
        // terminal has no children do not add and raise exception
        throw new UnsupportedOperationException("Cannot add children to terminal node");
    }

    @Override
    public String toString() {
        return token.getLexeme();
    }
}
