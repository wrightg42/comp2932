package com.sc18gtw.parser.exceptions;

import com.sc18gtw.lexer.Token;

/**
 * An exception for an Invalid token at the syntax analysis stage.
 */
public class InvalidTokenException extends Throwable {
    /**
     * Instantiates a new Invalid token exception.
     *
     * @param received the received token
     * @param expectedTokens the expected token lexemes
     */
    public InvalidTokenException(Token received, String... expectedTokens) {
        super();

        generateError(received, expectedTokens, false);
    }

    /**
     * Instantiates a new Invalid token exception.
     *
     * @param received the received token
     * @param expectedTypes the expected types of token
     */
    public InvalidTokenException(Token received, Token.TokenTypes... expectedTypes) {
        super();

        // convert types to strings
        expected = new String[expectedTypes.length];
        for (int i = 0; i < expectedTypes.length; ++i) {
            expected[i] = expectedTypes[i].name();
        }

        generateError(received, expected, true);
    }

    private String[] expected;
    private Token received;
    private String message;

    /**
     * Gets the token received by the parser.
     *
     * @return the received token.
     */
    public Token getToken() {
        return received;
    }

    /**
     * The array of strings that represent expected tokens.
     *
     * @implNote This will return a list of strings if a specific token was expected,
     *  or {@link com.sc18gtw.lexer.Token.TokenTypes} string values if a token type was expected.
     * @return the array of expected tokens.
     */
    public String[] getExpectedTokens() {
        return expected;
    }

    private void generateError(Token received, String[] expected, boolean areTypes) {
        this.expected = expected;
        this.received = received;

        // create string for expected
        StringBuilder expectedString = new StringBuilder();
        for (int i = 0; i < expected.length - 1; ++i) {
            if (areTypes) {
                expectedString.append(String.format("%s, ", expected[i]));
            } else {
                expectedString.append(String.format("\"%s\", ", expected[i]));
            }
        }

        if (expectedString.length() > 1) {
            expectedString.append("or ");
        }

        if (areTypes) {
            expectedString.append(String.format("%s", expected[expected.length - 1]));
        } else {
            expectedString.append(String.format("\"%s\"", expected[expected.length - 1]));
        }

        if (areTypes) {
            message = String.format("Line %d: Expected token(s) of type %s, got \"%s\" of type %s",
                received.getLine(), expectedString.toString(), received.getLexeme(), received.getType());
        } else {
            message = String.format("Line %d: Expected token(s) %s, got \"%s\"",
                    received.getLine(), expectedString.toString(), received.getType() != Token.TokenTypes.EOF ? received.getLexeme() : "<EOF>");
        }
    }

    @Override
    public String getMessage() {
        return message;
    }
}
