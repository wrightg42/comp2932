package com.sc18gtw.parser;

import com.sc18gtw.lexer.*;
import com.sc18gtw.lexer.exceptions.UnrecognisedTokensException;
import com.sc18gtw.parser.exceptions.InvalidTokenException;
import com.sc18gtw.parser.syntaxtree.*;

import java.io.FileNotFoundException;
import java.util.*;

/**
 * The Compiler's Syntactic Analyser.
 */
public class Parser {
    /**
     * Instantiates a new Parser.
     *
     * @param filename the filename of the source file for parser to analyse
     */
    public Parser(String filename) {
        this.filename = filename;
    }

    private String filename;
    private Lexer lexer;

    /**
     * Parse the source file to generate a syntax tree.
     *
     * @return the syntax tree
     * @throws FileNotFoundException       if the file was not found
     * @throws UnrecognisedTokensException if unrecognised tokens are detected in the source file
     * @throws InvalidTokenException       if invalid tokens are parsed
     */
    public SyntaxTree parseFile() throws FileNotFoundException, UnrecognisedTokensException, InvalidTokenException {
        SyntaxTree tree = new SyntaxTree(filename);
        lexer = new Lexer(filename);

        List<SyntaxTreeNode> classes = parseClasses();
        for (SyntaxTreeNode c: classes) {
            tree.addChild(c);
        }

        return tree;
    }

    private List<SyntaxTreeNode> parseClasses() throws UnrecognisedTokensException, InvalidTokenException {
        List<SyntaxTreeNode> classes = new LinkedList<>();

        // parse classes while there is more data in class
        while (lexer.peekNextToken().getType() != Token.TokenTypes.EOF) {
            classes.add(parseClass());
        }

        lexer.getNextToken(); // make lexer read EOF token and mark as no more tokens

        return classes;
    }

    private SyntaxTreeNode parseClass() throws UnrecognisedTokensException, InvalidTokenException {
        SyntaxTreeNode classNode = new SyntaxTreeNode(SyntaxTreeNode.SyntaxTypes.CLASS);

        tryParseToken("class");

        // get identifier
        Token nextToken = tryParseToken(Token.TokenTypes.IDENTIFIER);
        classNode.addChild(new SyntaxTreeTerminal(nextToken));

        tryParseToken("{");
        classNode.addChild(parseClassBody());
        tryParseToken("}");

        return classNode;
    }

    private SyntaxTreeNode parseClassBody() throws UnrecognisedTokensException, InvalidTokenException {
        SyntaxTreeNode bodyNode = new SyntaxTreeNode(SyntaxTreeNode.SyntaxTypes.CLASS_BODY);
        SyntaxTreeNode varsNode = new SyntaxTreeNode(SyntaxTreeNode.SyntaxTypes.CLASS_VARS);
        SyntaxTreeNode routinesNode = new SyntaxTreeNode(SyntaxTreeNode.SyntaxTypes.CLASS_ROUTINES);

        // read class body until closing brace or EOF
        Token nextToken = lexer.peekNextToken();
        while (!(nextToken.getLexeme().equals("}") || nextToken.getType() == Token.TokenTypes.EOF)) {
            if (nextToken.getType() == Token.TokenTypes.KEYWORD_CLASS_VAR_DECLARE) {
                varsNode.addChild(parseVar(false));
            } else if (nextToken.getType() == Token.TokenTypes.KEYWORD_METHOD_DECLARE) {
                routinesNode.addChild(parseClassRoutine());
            } else {
                // next token is not a variable or method declaration so error encountered
                throw new InvalidTokenException(nextToken, Token.TokenTypes.KEYWORD_CLASS_VAR_DECLARE, Token.TokenTypes.KEYWORD_METHOD_DECLARE);
            }

            nextToken = lexer.peekNextToken();
        }

        bodyNode.addChild(varsNode);
        bodyNode.addChild(routinesNode);

        return bodyNode;
    }

    private SyntaxTreeNode parseVar(boolean routine) throws UnrecognisedTokensException, InvalidTokenException {
        SyntaxTreeNode varNode;

        // parse correctly for class or method variable
        if (routine) {
            varNode = new SyntaxTreeNode(SyntaxTreeNode.SyntaxTypes.ROUTINE_VAR);
            Token nextToken = tryParseToken(Token.TokenTypes.KEYWORD_METHOD_VAR_DECLARE);
        } else {
            varNode = new SyntaxTreeNode(SyntaxTreeNode.SyntaxTypes.CLASS_VAR);
            Token nextToken = tryParseToken(Token.TokenTypes.KEYWORD_CLASS_VAR_DECLARE);
            varNode.addChild(new SyntaxTreeTerminal(nextToken));
        }

        // get variable type
        Token nextToken = tryParseToken(Token.TokenTypes.KEYWORD_TYPE, Token.TokenTypes.IDENTIFIER);
        varNode.addChild(new SyntaxTreeTerminal(nextToken));

        // get identifiers
        do {
            nextToken = tryParseToken(Token.TokenTypes.IDENTIFIER);
            varNode.addChild(new SyntaxTreeTerminal(nextToken));
            nextToken = tryParseToken(",", ";");
        } while (nextToken.getLexeme().equals(","));

        return varNode;
    }

    private SyntaxTreeNode parseClassRoutine() throws UnrecognisedTokensException, InvalidTokenException {
        SyntaxTreeNode routineNode = new SyntaxTreeNode(SyntaxTreeNode.SyntaxTypes.CLASS_ROUTINE);

        // get method deceleration type
        Token nextToken = tryParseToken(Token.TokenTypes.KEYWORD_METHOD_DECLARE);
        routineNode.addChild(new SyntaxTreeTerminal(nextToken));

        // get return type
        nextToken = tryParseToken(Token.TokenTypes.KEYWORD_TYPE, Token.TokenTypes.IDENTIFIER, Token.TokenTypes.KEYWORD_VOID);
        routineNode.addChild(new SyntaxTreeTerminal(nextToken));

        // get identifier
        nextToken = tryParseToken(Token.TokenTypes.IDENTIFIER);
        routineNode.addChild(new SyntaxTreeTerminal(nextToken));

        tryParseToken("(");
        routineNode.addChild(parseParamList());
        tryParseToken(")");
        routineNode.addChild(parseRoutineBody());

        return routineNode;
    }

    private SyntaxTreeNode parseParamList() throws UnrecognisedTokensException, InvalidTokenException {
        SyntaxTreeNode paramsNode = new SyntaxTreeNode(SyntaxTreeNode.SyntaxTypes.PARAMS_LIST);

        // read while more params are available in list
        boolean moreParams = lexer.peekNextToken().getType() == Token.TokenTypes.KEYWORD_TYPE || lexer.peekNextToken().getType() == Token.TokenTypes.IDENTIFIER;
        while (moreParams) {
            SyntaxTreeNode paramNode = new SyntaxTreeNode(SyntaxTreeNode.SyntaxTypes.PARAM);

            // get param type
            Token nextToken = tryParseToken(Token.TokenTypes.KEYWORD_TYPE, Token.TokenTypes.IDENTIFIER);
            paramNode.addChild(new SyntaxTreeTerminal(nextToken));

            // get param identifier
            nextToken = tryParseToken(Token.TokenTypes.IDENTIFIER);
            paramNode.addChild(new SyntaxTreeTerminal(nextToken));

            paramsNode.addChild(paramNode);

            // check for more params in list
            if (lexer.peekNextToken().getLexeme().equals(",")) {
                lexer.getNextToken();
            } else {
                moreParams = false;
            }
        }

        return paramsNode;
    }

    private SyntaxTreeNode parseRoutineBody() throws UnrecognisedTokensException, InvalidTokenException {
        SyntaxTreeNode bodyNode = new SyntaxTreeNode(SyntaxTreeNode.SyntaxTypes.ROUTINE_BODY);

        tryParseToken("{");

        // read till end or routine body
        Token nextToken = lexer.peekNextToken();
        while (!(nextToken.getLexeme().equals("}") || nextToken.getType() == Token.TokenTypes.EOF)) {
            if (nextToken.getType() == Token.TokenTypes.KEYWORD_METHOD_VAR_DECLARE) {
                bodyNode.addChild(parseVar(true));
            } else if (nextToken.getType() == Token.TokenTypes.KEYWORD_STATEMENT && !nextToken.getLexeme().equals("else")) {
                switch (nextToken.getLexeme()) {
                    case "let":
                        bodyNode.addChild(parseLetStatement());
                        break;
                    case "if":
                        bodyNode.addChild(parseIfStatement());
                        break;
                    case "while":
                        bodyNode.addChild(parseWhileStatement());
                        break;
                    case "do":
                        bodyNode.addChild(parseDoStatement());
                        break;
                    case "return":
                        bodyNode.addChild(parseReturnStatement());
                        break;
                }
            } else {
                // not variable declaration or statement so error encountered
                throw new InvalidTokenException(nextToken, Token.TokenTypes.KEYWORD_METHOD_VAR_DECLARE, Token.TokenTypes.KEYWORD_STATEMENT);
            }

            nextToken = lexer.peekNextToken();
        }

        tryParseToken("}");

        return bodyNode;
    }

    private SyntaxTreeNode parseLetStatement() throws UnrecognisedTokensException, InvalidTokenException {
        SyntaxTreeNode letNode = new SyntaxTreeNode(SyntaxTreeNode.SyntaxTypes.LET);

        tryParseToken("let");

        // get identifier of variable to change
        Token nextToken = tryParseToken(Token.TokenTypes.IDENTIFIER);
        letNode.addChild(new SyntaxTreeTerminal(nextToken));

        nextToken = tryParseToken("[", "=");

        // parse expression for array element's
        if (nextToken.getLexeme().equals("[")) {
            letNode.addChild(parseExpression());

            tryParseToken("]");
            tryParseToken("=");
        }

        letNode.addChild(parseExpression());
        tryParseToken(";");

        return letNode;
    }

    private SyntaxTreeNode parseIfStatement() throws UnrecognisedTokensException, InvalidTokenException {
        SyntaxTreeNode ifNode = new SyntaxTreeNode(SyntaxTreeNode.SyntaxTypes.IF);

        tryParseToken("if");
        tryParseToken("(");
        ifNode.addChild(parseExpression());
        tryParseToken(")");
        ifNode.addChild(parseRoutineBody());

        // check for else clause in if statement
        if (lexer.peekNextToken().getLexeme().equals("else")) {
            lexer.getNextToken();
            ifNode.addChild(parseRoutineBody());
        }

        return ifNode;
    }

    private SyntaxTreeNode parseWhileStatement() throws UnrecognisedTokensException, InvalidTokenException {
        SyntaxTreeNode whileNode = new SyntaxTreeNode(SyntaxTreeNode.SyntaxTypes.WHILE);

        tryParseToken("while");
        tryParseToken("(");
        whileNode.addChild(parseExpression());
        tryParseToken(")");
        whileNode.addChild(parseRoutineBody());

        return whileNode;
    }

    private SyntaxTreeNode parseDoStatement() throws UnrecognisedTokensException, InvalidTokenException {
        SyntaxTreeNode doNode = new SyntaxTreeNode(SyntaxTreeNode.SyntaxTypes.DO);

        tryParseToken("do");

        // get identifier
        Token nextToken = tryParseToken(Token.TokenTypes.IDENTIFIER);
        doNode.addChild(new SyntaxTreeTerminal(nextToken));

        nextToken = tryParseToken(".", "(");

        // check for second identifier due to class methods
        if (nextToken.getLexeme().equals(".")) {
            nextToken = tryParseToken(Token.TokenTypes.IDENTIFIER);
            doNode.addChild(new SyntaxTreeTerminal(nextToken));

            tryParseToken("(");
        }

        doNode.addChild(parseExpressionList());
        tryParseToken(")");
        tryParseToken(";");

        return doNode;
    }

    private SyntaxTreeNode parseReturnStatement() throws UnrecognisedTokensException, InvalidTokenException {
        SyntaxTreeNode returnNode = new SyntaxTreeNode(SyntaxTreeNode.SyntaxTypes.RETURN);

        tryParseToken("return");

        // check if expression to return
        if (!lexer.peekNextToken().getLexeme().equals(";")) {
            returnNode.addChild(parseExpression());
        }

        tryParseToken(";");

        return returnNode;
    }

    private SyntaxTreeNode parseExpressionList() throws UnrecognisedTokensException, InvalidTokenException {
        SyntaxTreeNode expressionListNode = new SyntaxTreeNode(SyntaxTreeNode.SyntaxTypes.EXPRESSION_LIST);

        // check if there are more expressions to parse
        boolean moreExpressions = !lexer.peekNextToken().getLexeme().equals(")");
        while (moreExpressions) {
            expressionListNode.addChild(parseExpression());

            // check for more expressions to parse
            if (lexer.peekNextToken().getLexeme().equals(",")) {
                lexer.getNextToken();
            } else {
                moreExpressions = false;
            }
        }

        return expressionListNode;
    }

    private SyntaxTreeNode parseExpression() throws UnrecognisedTokensException, InvalidTokenException {
        SyntaxTreeNode base = new SyntaxTreeNode(SyntaxTreeNode.SyntaxTypes.EXPRESSION);
        SyntaxTreeNode nextExpression = parseRelationalExpression();

        // check if there are more expressions to come
        if ("&|".contains(lexer.peekNextToken().getLexeme())) {
            Token nextToken = tryParseToken("&", "|");
            SyntaxTreeNode operation = new SyntaxTreeOperationNode(nextToken);
            base.addChild(operation);
            operation.addChild(nextExpression);
            nextExpression = parseExpression(); // we don't want more EXPRESSION nodes in the tree
            operation.addChild(nextExpression.getChild(0));
        } else {
            base.addChild(nextExpression);
        }

        return base;
    }

    private SyntaxTreeNode parseRelationalExpression() throws UnrecognisedTokensException, InvalidTokenException {
        SyntaxTreeNode base;
        SyntaxTreeNode nextExpression = parseArithmeticExpression();

        // check if there are more relational expressions to come
        if ("=<>".contains(lexer.peekNextToken().getLexeme())) {
            Token nextToken = tryParseToken("=", "<", ">");
            base = new SyntaxTreeOperationNode(nextToken);
            base.addChild(nextExpression);
            base.addChild(parseRelationalExpression());
        } else {
            base = nextExpression;
        }

        return base;
    }

    private SyntaxTreeNode parseArithmeticExpression() throws UnrecognisedTokensException, InvalidTokenException {
        SyntaxTreeNode base;
        SyntaxTreeNode nextExpression = parseTerm();

        // check if there are more arithmetic expressions to come
        if ("+-".contains(lexer.peekNextToken().getLexeme())) {
            Token nextToken = tryParseToken("+", "-");
            base = new SyntaxTreeOperationNode(nextToken);
            base.addChild(nextExpression);
            base.addChild(parseArithmeticExpression());
        } else {
            base = nextExpression;
        }

        return base;
    }

    private SyntaxTreeNode parseTerm() throws UnrecognisedTokensException, InvalidTokenException {
        SyntaxTreeNode base;
        SyntaxTreeNode nextExpression = parseFactor();

        // check if there are more terms to come
        if ("*/".contains(lexer.peekNextToken().getLexeme())) {
            Token nextToken = tryParseToken("*", "/");
            base = new SyntaxTreeOperationNode(nextToken);
            base.addChild(nextExpression);
            base.addChild(parseTerm());
        } else {
            base = nextExpression;
        }

        return base;
    }

    private SyntaxTreeNode parseFactor() throws UnrecognisedTokensException, InvalidTokenException {
        SyntaxTreeNode base;

        // check if there is an operator before the factor
        if ("-~".contains(lexer.peekNextToken().getLexeme())) {
            Token nextToken = tryParseToken("-", "~");
            base = new SyntaxTreeOperationNode(nextToken);
            base.addChild(parseOperand());
        } else {
            base = parseOperand();
        }

        return base;
    }

    private SyntaxTreeNode parseOperand() throws UnrecognisedTokensException, InvalidTokenException {
        SyntaxTreeNode nextNode = new SyntaxTreeNode(SyntaxTreeNode.SyntaxTypes.OPERAND_DATA);
        Token nextToken = lexer.peekNextToken();
        if (nextToken.getLexeme().equals("(")) {
            // check for expression as operand
            tryParseToken("(");
            nextNode = parseExpression();
            tryParseToken(")");
        } else {
            // parse operand
            nextToken = tryParseToken(Token.TokenTypes.NUMBER, Token.TokenTypes.IDENTIFIER, Token.TokenTypes.STRING,
                    Token.TokenTypes.KEYWORD_BOOLEAN, Token.TokenTypes.KEYWORD_NULL, Token.TokenTypes.KEYWORD_OBJECT_REFERENCE);

            // check for array data/function call
            boolean arrayOrFunction = false;
            if (nextToken.getType() == Token.TokenTypes.IDENTIFIER) {
                if (lexer.peekNextToken().getLexeme().equals(".")) {
                    // member of class check
                    arrayOrFunction = true;
                    nextNode.addChild(new SyntaxTreeTerminal(nextToken));
                    lexer.getNextToken();
                    nextToken = tryParseToken(Token.TokenTypes.IDENTIFIER);
                    nextNode.addChild(new SyntaxTreeTerminal(nextToken));
                }

                if (lexer.peekNextToken().getLexeme().equals("[")) {
                    // array check
                    if (!arrayOrFunction) {
                        arrayOrFunction = true;
                        nextNode.addChild(new SyntaxTreeTerminal(nextToken));
                    }
                    lexer.getNextToken();
                    nextNode.addChild(parseExpression());
                    tryParseToken("]");
                } else if (lexer.peekNextToken().getLexeme().equals("(")) {
                    // function check
                    if (!arrayOrFunction) {
                        arrayOrFunction = true;
                        nextNode.addChild(new SyntaxTreeTerminal(nextToken));
                    }
                    lexer.getNextToken();
                    nextNode.addChild(parseExpressionList());
                    tryParseToken(")");
                }
            }

            if (!arrayOrFunction) {
                nextNode = new SyntaxTreeTerminal(nextToken);
            }
        }

        return nextNode;
    }

    private Token tryParseToken(Token.TokenTypes... wantedTypes) throws UnrecognisedTokensException, InvalidTokenException {
        Token nextToken = lexer.peekNextToken();
        for (Token.TokenTypes t: wantedTypes) {
            if (nextToken.getType() == t) {
                return lexer.getNextToken();
            }
        }

        throw new InvalidTokenException(nextToken, wantedTypes);
    }

    private Token tryParseToken(String... wantedTokens) throws UnrecognisedTokensException, InvalidTokenException {
        Token nextToken = lexer.peekNextToken();
        for (String t: wantedTokens) {
            if (nextToken.getLexeme().equals(t)) {
                return lexer.getNextToken();
            }
        }

        throw new InvalidTokenException(nextToken, wantedTokens);
    }
}
