package com.sc18gtw.lexer;

/**
 * Represents a Token.
 *
 * @author George Wright
 */
public class Token {
    /**
     * The enum for different Token types.
     */
    public enum TokenTypes {
        /**
         * Class keyword token type.
         */
        KEYWORD_CLASS,
        /**
         * Method declaration token type.
         */
        KEYWORD_METHOD_DECLARE,
        /**
         * Type keyword token type.
         */
        KEYWORD_TYPE,
        /**
         * Void token type.
         */
        KEYWORD_VOID,
        /**
         * Variable declaration token type.
         */
        KEYWORD_CLASS_VAR_DECLARE,
        /**
         * Variable declaration token type.
         */
        KEYWORD_METHOD_VAR_DECLARE,
        /**
         * Statement token type.
         */
        KEYWORD_STATEMENT,
        /**
         * Object reference token type.
         */
        KEYWORD_OBJECT_REFERENCE,
        /**
         * Boolean token type.
         */
        KEYWORD_BOOLEAN,
        /**
         * Null token type.
         */
        KEYWORD_NULL,
        /**
         * Identifier token type.
         */
        IDENTIFIER,
        /**
         * String token type.
         */
        STRING,
        /**
         * Number token type.
         */
        NUMBER,
        /**
         * Operator token type.
         */
        OPERATOR,
        /**
         * Punctuator token type.
         */
        PUNCTUATOR,
        /**
         * End Of File token type.
         */
        EOF,
        /**
         * Error token type.
         */
        ERROR
    }

    /**
     * Instantiates a new Token.
     *
     * @param lexeme the token's lexeme
     * @param type   the token's type
     * @param line   the token's line
     */
    public Token(String lexeme, TokenTypes type, int line) {
        this.lexeme = lexeme;
        this.type = type;
        this.line = line;
    }

    private String lexeme;
    private TokenTypes type;
    private int line;

    /**
     * Gets the lexeme.
     *
     * @return the lexeme
     */
    public String getLexeme() {
        return lexeme;
    }

    /**
     * Gets the token's type.
     *
     * @return the type of the token
     */
    public TokenTypes getType() {
        return type;
    }

    /**
     * Gets token's line.
     *
     * @return the line of the token
     */
    public int getLine() {
        return line;
    }

    @Override
    public String toString() {
        return String.format("<\"%s\", %s, %d>", lexeme, type, line);
    }
}
