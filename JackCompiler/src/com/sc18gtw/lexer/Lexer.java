package com.sc18gtw.lexer;

import com.sc18gtw.lexer.exceptions.UnrecognisedTokenException;
import com.sc18gtw.lexer.exceptions.UnrecognisedTokensException;

import java.io.*;
import java.util.*;
import java.util.regex.Pattern;

/**
 * The Compiler's Lexical Analyser.
 */
public class Lexer {
    /**
     * Instantiates a new Lexer.
     *
     * @param filename the filename of the source file for lexer to analyse
     * @throws FileNotFoundException if the file was not found
     */
    public Lexer(String filename) throws FileNotFoundException {
        fileReader = new PushbackReader(new FileReader((new File(filename))), 2);
        moreTokens = true;
        nextTokenAvailable = false;
        nextToken = null;
        line = 1;
    }

    private Boolean moreTokens;
    private PushbackReader fileReader;
    private Boolean nextTokenAvailable;
    private Token nextToken;
    private int line;

    // constants for key tokens
    private static final List<String> CLASS_KEYWORDS = Arrays.asList("class");
    private static final List<String> METHOD_KEYWORDS = Arrays.asList("constructor", "method", "function");
    private static final List<String> TYPE_KEYWORDS = Arrays.asList("int", "boolean", "char");
    private static final List<String> VOID_KEYWORDS = Arrays.asList("void");
    private static final List<String> CLASS_VAR_KEYWORDS = Arrays.asList("static", "field");
    private static final List<String> METHOD_VAR_KEYWORDS = Arrays.asList("var");
    private static final List<String> STATEMENT_KEYWORDS = Arrays.asList("let", "do", "if", "else", "while", "return");
    private static final List<String> REFERENCE_KEYWORDS = Arrays.asList("this");
    private static final List<String> BOOLEAN_KEYWORDS = Arrays.asList("true", "false");
    private static final List<String> NULL_KEYWORDS = Arrays.asList("null");
    private static final String OPERATOR_SYMBOLS = "=+-*/&|~<>";
    private static final String PUNCTUATOR_SYMBOLS = "()[]{},.;";

    /**
     * Gets the next token from the source file.
     *
     * @return the next token
     * @throws UnrecognisedTokensException if unrecognised tokens are detected
     */
    public Token getNextToken() throws UnrecognisedTokensException {
        if (moreTokens) {
            if (!nextTokenAvailable) {
                readNextToken();
            }

            nextTokenAvailable = false;
            if (nextToken.getType() == Token.TokenTypes.EOF) {
                // state there are no more tokens
                moreTokens = false;
            } else if (nextToken.getType() == Token.TokenTypes.ERROR) {
                // throw error if there was one
                throwErrorTokens();
            }

            return nextToken;
        }

        return null;
    }

    /**
     * Peeks next token from the source file without consuming it.
     *
     * @return the next token
     * @throws UnrecognisedTokensException if unrecognised tokens are detected
     */
    public Token peekNextToken() throws UnrecognisedTokensException {
        if (moreTokens) {
            if (!nextTokenAvailable) {
                readNextToken();
            }

            // throw error if there was one
            if (nextToken.getType() == Token.TokenTypes.ERROR) {
                throwErrorTokens();
            }

            return nextToken;
        }

        return null;
    }

    private void readNextToken() {
        try {
            // variables to keep track of next token
            String lexeme = "";
            Token.TokenTypes type = Token.TokenTypes.ERROR;

            // skip whitespace before next lexeme
            readRegex("[\\s]*");

            int c = fileReader.read(); // read first char
            fileReader.unread(c); // push c back to buffer for correct reading of lexeme

            if (c == -1) {
                // EOF reached since -1 value read, close stream and set token
                fileReader.close();
                type = Token.TokenTypes.EOF;
            } else if (c == '"') {
                lexeme = readRegex("^.{0,1}$|^\".*[^\"\n]$");
                lexeme += (char) fileReader.read(); // last character should be consumed (")
                if (lexeme.charAt(lexeme.length() - 1) == '"') {
                    type = Token.TokenTypes.STRING;
                }
            } else if (Character.isDigit(c)) {
                lexeme = readRegex("\\d*");
                type = Token.TokenTypes.NUMBER;
            } else if (Character.isLetterOrDigit(c) || c == '_') {
                lexeme = readRegex("[\\w_]*");
                type = getIdentifierType(lexeme);
            } else if (OPERATOR_SYMBOLS.indexOf(c) != -1) {
                lexeme += (char) fileReader.read();
                type = Token.TokenTypes.OPERATOR;

                // check for comment
                if (c == '/') {
                    c = fileReader.read();
                    fileReader.unread(c); // unread the next character so comment string can be formed properly
                    if (c == '/' || c == '*') {
                        fileReader.unread('/'); // push back / so full comment string formed
                        readRegex("(?s)^.{0,2}$|^\\/\\*.*(?<!\\*\\/)$|^\\/\\/.*[^\\n]$");
                        fileReader.read(); // last character should be consumed (newline, or /)
                        readNextToken();
                        return;
                    }
                }
            } else if (PUNCTUATOR_SYMBOLS.indexOf(c) != -1) {
                lexeme += (char) fileReader.read();
                type = Token.TokenTypes.PUNCTUATOR;
            } else {
                // error symbol
                lexeme += (char) fileReader.read();
            }

            nextToken = new Token(lexeme, type, line);
            nextTokenAvailable = true;
        } catch (IOException e) {
            // IO Exception so no valid token
            nextToken = null;
            nextTokenAvailable = false;
        }
    }

    private String readRegex(String pattern) throws IOException {
        int c;
        StringBuilder lexeme = new StringBuilder();
        //Pattern p = Pattern.compile(pattern, Pattern.MULTILINE);

        // read till end of regex
        do {
            c = fileReader.read();
            lexeme.append((char) c);

            // keep line number updated
            if (c == '\n') {
                ++line;
            }
        } while (c != -1 && lexeme.toString().matches(pattern));

        if (c != -1) {
            fileReader.unread(c); // push character that is not part of regex back to buffer
        }
        lexeme.setLength(lexeme.length() - 1);
        return lexeme.toString();
    }

    public Token.TokenTypes getIdentifierType(String lexeme) {
        if (CLASS_KEYWORDS.contains(lexeme)) {
            return Token.TokenTypes.KEYWORD_CLASS;
        } else if (METHOD_KEYWORDS.contains(lexeme)) {
            return Token.TokenTypes.KEYWORD_METHOD_DECLARE;
        } else if (TYPE_KEYWORDS.contains(lexeme)) {
            return Token.TokenTypes.KEYWORD_TYPE;
        } else if (VOID_KEYWORDS.contains(lexeme)) {
            return Token.TokenTypes.KEYWORD_VOID;
        } else if (CLASS_VAR_KEYWORDS.contains(lexeme)) {
            return Token.TokenTypes.KEYWORD_CLASS_VAR_DECLARE;
        } else if (METHOD_VAR_KEYWORDS.contains(lexeme)) {
            return Token.TokenTypes.KEYWORD_METHOD_VAR_DECLARE;
        } else if (STATEMENT_KEYWORDS.contains(lexeme)) {
            return Token.TokenTypes.KEYWORD_STATEMENT;
        } else if (REFERENCE_KEYWORDS.contains(lexeme)) {
            return Token.TokenTypes.KEYWORD_OBJECT_REFERENCE;
        } else if (BOOLEAN_KEYWORDS.contains(lexeme)) {
            return Token.TokenTypes.KEYWORD_BOOLEAN;
        } else if (NULL_KEYWORDS.contains(lexeme)) {
            return Token.TokenTypes.KEYWORD_NULL;
        } else {
            return Token.TokenTypes.IDENTIFIER;
        }
    }

    private void throwErrorTokens() throws UnrecognisedTokensException {
        List<UnrecognisedTokenException> exceptions = new LinkedList<>();
        while (nextToken.getType() != Token.TokenTypes.EOF) {
            if (nextToken.getType() == Token.TokenTypes.ERROR) {
                exceptions.add(new UnrecognisedTokenException(nextToken));
            }

            readNextToken();
        }

        UnrecognisedTokenException[] exceptionsArr = new UnrecognisedTokenException[exceptions.size()];
        exceptions.toArray(exceptionsArr);
        throw new UnrecognisedTokensException(exceptionsArr);
    }
}