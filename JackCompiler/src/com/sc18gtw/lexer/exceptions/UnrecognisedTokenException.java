package com.sc18gtw.lexer.exceptions;

import com.sc18gtw.lexer.Token;

/**
 * An exception for an Unrecognised token at lexical analysis stage.
 */
public class UnrecognisedTokenException extends Throwable {
    /**
     * Instantiates a new Unrecognised token exception.
     *
     * @param token the unrecognised token received
     */
    public UnrecognisedTokenException(Token token) {
        super(String.format("Line %d: Unrecognised token \"%s\"", token.getLine(), token.getLexeme()));

        this.token = token;
    }

    private Token token;

    /**
     * Gets the unrecognised token that raised the exception.
     *
     * @return the unrecognised token
     */
    public Token getToken() {
        return token;
    }
}
