package com.sc18gtw;

import com.sc18gtw.generator.Generator;

import java.io.IOException;
import java.util.List;

/**
 * The Main class.
 */
public class Main {
    /**
     * The entry point of application.
     *
     * @param args the input arguments
     * @throws IOException the io exception from reading files
     */
    public static void main(String[] args) throws IOException {
        if (args.length != 1) {
            System.out.println("Must input a parameter to compile a source folder...\nPress enter to continue...");
            System.in.read();
        } else {
            String path = args[0];
            compileProject(path);
        }
    }

    private static void compileProject(String path) throws IOException {
        System.out.println(String.format("Compiling %s...", path));

        try {
            List<Throwable> warnings = Generator.compile(path);
            System.out.println("Compiled successfully...\n");
            if (warnings.size() > 0) {
                System.out.println("The following warnings occurred during compilation...");
                for (Throwable e : warnings) {
                    System.out.println(e.getMessage());
                }
            }
        } catch (Throwable e) {
            System.out.println("Error occurred during compilation...");
            System.out.println(e.getMessage());
            System.out.println("\nTerminating compilation...");
        }

        System.out.println("Press enter to continue...");
        System.in.read();
    }
}
